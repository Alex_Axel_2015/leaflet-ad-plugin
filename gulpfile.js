var gulp        = require('gulp'),
    inject      = require('gulp-inject'),
    ngAnnotate  = require('gulp-ng-annotate'),
    concat      = require('gulp-concat'),
    replace     = require('gulp-replace'),
    htmlmin     = require('gulp-htmlmin'),
    tcache      = require('gulp-angular-templatecache');
    uglify      = require('gulp-uglify'), 
    babel       = require('gulp-babel'),
    rename      = require('gulp-rename'),
    header      = require('gulp-header'),
    sourcemaps  = require('gulp-sourcemaps'),
    cssmin      = require('gulp-cssmin');

var config = {
    srcTemplates:[
        'src/scripts/components/*.html',
        'src/scripts/controls/*.html'
    ],
    destPartials: 'src/scripts'
};

var info = ['/*\nleaflet-ad-plugin 9.4.8, ',
            'a plugin that adds drawing and ', 
            'editing tools to Leaflet powered maps.',
            '\n(c) 2017-2018, Alexander Dudin\n*/\n\n'].join('');

gulp.task('html-templates', function() {
    return gulp.src(config.srcTemplates)
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(tcache('app.templates.js', {
            module: 'templateCache', standalone:true
        }))
        .pipe(gulp.dest(config.destPartials));
});

gulp.task('index', function () {
  var target = gulp.src('./test/index.html');
  var sources = gulp.src([
      './test/scripts/libs.js',
      './dist/leaflet-ad-plugin.min.js',
      './test/scripts/app.js',
      './test/layouts/app.css',
      './dist/leaflet-ad-plugin.min.css',
      './test/layouts/styles.css'
  ], {read: false});
 
  return target.pipe(inject(sources))
    .pipe(replace('/dist/', '../dist/'))
    .pipe(replace('/test/', '../test/'))
    .pipe(gulp.dest('./test'));
});

gulp.task('scripts', function(){
    return gulp.src([        
        'node_modules/angular/angular.min.js',      
        'node_modules/angular-route/angular-route.min.js',
        'bower_components/leaflet/dist/leaflet.js'
    ])
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('test/scripts'));
});

gulp.task('links', function(){
    return gulp.src([
        'bower_components/leaflet/dist/leaflet.css'
    ])
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('test/layouts'));
});

gulp.task('releaze', function(){
    return gulp.src([
        'src/scripts/app.module.js',
        'src/scripts/app.config.js',
        'src/scripts/app.controller.js',
        'src/scripts/app.constant.js',
        'src/scripts/app.templates.js',
        '!src/scripts/**/*.spec.js',
        'src/scripts/services/*.js',
        'src/scripts/page.js',
        'src/scripts/components/*.js'
    ])
    .pipe(concat('app.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('test/scripts'));
});

gulp.task('adcss', function(){
    gulp.src('src/sass/leaflet-ad-plugin.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist'));
});

gulp.task('ad', function(){
    return gulp.src([
        'src/scripts/leaflet-ad-plugin/includes/variables.js',
        'src/scripts/leaflet-ad-plugin/L.AD.js',
        'src/scripts/leaflet-ad-plugin/L.AD.Map.js',
        // 'src/scripts/leaflet-ad-plugin/includes/DragMixin.js', // test
        // 'src/scripts/leaflet-ad-plugin/includes/Snapping.js', // test
        'src/scripts/leaflet-ad-plugin/includes/L.AD.Comand.js',
        'src/scripts/leaflet-ad-plugin/includes/L.AD.Control.js',
        'src/scripts/leaflet-ad-plugin/includes/L.AD.Tooltip.js',
        'src/scripts/leaflet-ad-plugin/Draw/L.AD.Draw.js',
        'src/scripts/leaflet-ad-plugin/Edit/L.AD.Edit.js',
        'src/scripts/leaflet-ad-plugin/**/*.js'
    ])    
    .pipe(sourcemaps.init())
    .pipe(babel({
        presets: ['es2015']
    }))
    .pipe(concat('leaflet-ad-plugin.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('dist'));
});

gulp.task('version', function(){
    return gulp.src('dist/leaflet-ad-plugin.min.js')    
    .pipe(header(info))
    .pipe(gulp.dest('dist'));
});