(function () {
    'use strict';

    function EmptyController($scope, $templateCache) {
        var map,
            overlay = L.featureGroup(),
            overlayDraw = L.featureGroup(),
            drawControlDrawOnly,
            drawControlEditOnly,
            toolbar,
            center = [50.50625, 45.462109375];

        $scope.init = init;

        $scope.init();

        var polygonLayer = new L.GeoJSON();

        map.on('ad:drawstart', function () {
            // console.log('ad: drawstart');
        });

        map.on('ad:markerdragend', function () {
            console.log('ad: markerdragend');
        });

        map.on('layeradd', function (layer, layername) {
            // console.log('layeradd');
            // console.log(map);       
        });

        // map.on('mousemove', function (e) {

        // });

        map.on('ad:create', function (e) {
            // console.log('ad: create');

            // console.log(map);

            var type = e.layerType,
                layer = e.layer;

            // overlayDraw.clearLayers();
            // L.geoJson(polygonLayer.toGeoJSON(), {
            //     onEachFeature: function (feature, layer) {
            //         if (layer.getLayers) {
            //             layer.getLayers().forEach(function (l) {
            //                 overlayDraw.addLayer(l);
            //             })
            //         } else {
            //             overlayDraw.addLayer(layer);
            //         }
            //     }
            // });

            // overlayDraw.addLayer(layer);
            // if (!!polygonLayer._map == true) map.removeLayer(polygonLayer);
            // polygonLayer = L.geoJson(overlayDraw.toGeoJSON());

            // map.removeLayer(e.layer);
            // polygonLayer.addTo(map);

            ///////////////////////////////////////////////////////
            // polygonLayer.ad.enable();

            // map.ad.ComandDraw.undo();
            // map.ad.ComandDraw.redo();

            // log.add('\nValue: '+map.ad.ComandDraw.getCurrentValue()); log.show();
            // map.ad.comandDraw = new L.AD.Comand();
            // map.ad.enableDraw('Poly', {});
        });

        ///////////////////////////////////////////

        function init() {
            map = L.map('map', {
                center: center,
                zoom: 4,
                closePopupOnClick: false,
                doubleClickZoom: false,
                inertia: false
            });

            // L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            L.tileLayer('https://api.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiY3RpcHBldHQiLCJhIjoiS3lpTnN4MCJ9.YG_uH8r7IgwgcSWEPYROMA', {
                attribution: '© TEST.',
                maxZoom: 18
            }).addTo(map);

            map.ad.setPathOptions({
                className: 'pathOptionsCustomClass',
                fillColor: 'dodgerblue',
                fillOpacity: 0.4
            });

            map.ad.toggleGlobalEditMode();

            map.on('mousedown', function (e) {
                e.originalEvent.which == 1 ? map.dragging.disable() : null;
            });

            map.on('mouseup', function (e) {
                map.dragging.enable();
            });

            //
            L.control.custom({
                position: 'topleft',
                content: $templateCache.get('editor.control.html'),
                classes: 'panel panel-default',
                style:
                {
                    width: '300px',
                    margin: '20px',
                    padding: '0px',
                },
                events:
                {
                    click: function (data) {
                        function updateEditMode() {
                            map.ad.toggleGlobalEditMode();
                            map.ad.toggleGlobalEditMode();
                        }
                        function classActive(_this, _className, isEdit) {
                            const editMode = ['move', 'remove'],
                                drawMode = ['polyline', 'polygon', 'circle'];

                            if (isEdit) {
                                for (var key in editMode) {
                                    var element = _this.getElementsByClassName(editMode[key])[0].classList;
                                    if (editMode[key] == _className) {
                                        element.toggle('active');
                                        map.ad.Draw['is_' + editMode[key]] = !map.ad.Draw['is_' + editMode[key]];
                                    } else {
                                        element.remove('active');
                                        map.ad.Draw['is_' + editMode[key]] = false;
                                    }
                                }
                            }else{
                                for (var key in drawMode) {
                                    var element = _this.getElementsByClassName(drawMode[key])[0].classList;
                                    drawMode[key] == _className ? element.toggle('active') : element.remove('active');
                                }
                            }
                        }
                        if (data.target.localName === 'button') {
                            var className = data.target.className.split(' ')[0];
                            switch (className) {
                                case 'move':
                                case 'remove': classActive(this, className, true); break;
                                case 'polyline': classActive(this, className); map.ad.enableDraw('Line'); break;
                                case 'polygon': classActive(this, className); map.ad.enableDraw('Poly'); break;
                                case 'circle': classActive(this, className); map.ad.enableDraw('Circle'); break;
                                case 'undo': map.ad.ComandDraw.undo(); updateEditMode(); break;
                                case 'redo': map.ad.ComandDraw.redo(); updateEditMode(); break;
                                default: console.log(data.target.className);
                            }
                        }
                    }
                }
            }).addTo(map);
            //

            console.log('init');

            // L.geoJson(data2, {
            //     onEachFeature: function (feature, layer) {
            //         if (layer.getLayers) {
            //             layer.getLayers().forEach(function (l) {
            //                 overlayDraw.addLayer(l);
            //             })
            //         } else {
            //             overlayDraw.addLayer(layer);
            //         }
            //     }
            // });

            // polygonLayer = L.geoJson(overlayDraw.toGeoJSON()).addTo(map);

            // L.geoJson(data, {
            //     onEachFeature: function (feature, layer) {
            //         if (layer.getLayers) {
            //             layer.getLayers().forEach(function (l) {
            //                 overlayDraw.addLayer(l);
            //             })
            //         } else {
            //             overlayDraw.addLayer(layer);
            //         }
            //     }
            // });

            // map.removeLayer(polygonLayer);
            // polygonLayer = L.geoJson(overlayDraw.toGeoJSON());//.addTo(map);
            // polygonLayer.ad.enable();

            // console.log(polygonLayer.toGeoJSON());
            // map.ad.enableDraw('Line', {});
            map.ad.enableDraw('Circle', {});
        }
    }

    angular
        .module('app')
        .component('empty', {
            template: ['$templateCache', function ($templateCache) {
                return $templateCache.get('empty.component.html')
            }],
            controller: ['$scope', '$templateCache', EmptyController],
            bindings: {
                model: '=?'
            }
        });

})();