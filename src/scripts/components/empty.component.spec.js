describe('Component: Empty', function () {
    beforeEach(module('app'));
    describe('EmptyController', function () {
        var $rootScope, $componentController, $controller, $scope;
        beforeEach(inject(function (_$rootScope_, $injector) {
            $rootScope = _$rootScope_;
            $scope = $rootScope.$new();
            $componentController = $injector.get('$componentController');
            $controller = $componentController('empty', { $scope: $scope });                     
        }));

        it('test: 2+2', function () {
            expect(2 + 2).toEqual(4);
        });

        it('should bind to the correct item', function () {            
            expect($scope.test).toEqual(2);
        });
    });
});