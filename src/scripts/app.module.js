(function() {
    'use strict';

    angular
        .module('app', [
            'ngRoute',
            'templateCache',
            'app.home'
        ]);

})();