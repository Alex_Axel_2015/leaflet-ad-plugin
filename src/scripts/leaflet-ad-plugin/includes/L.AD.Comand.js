(function () {
    'use strict';

    L.AD.Comand = function () {
        var raises = [],
            commands = [];

        var options = {
            depth: 100
        };

        function action(command) {
            var name = command.execute.toString().substr(9, 3);
            return name.charAt(0).toUpperCase() + name.slice(1);
        }

        return {
            execute: function (command) {
                command.execute();
                commands.push(command);
                raises = [];

                var count_commands = commands.length;
                for (var c in commands) if (!!c['log']) count_commands--;
                while (count_commands > options.depth) {
                    var unc = commands.splice(0, 1);
                    if (!unc['log']) count_commands--;
                }
            },

            undo: function () {
                var command;

                do {
                    command = commands.pop();
                } while (!!command && !!command.log);

                // var command = commands.pop();

                if (!!command) {
                    try {
                        command.undo();
                        raises.push(command);
                    } catch (err) {
                        console.log(err);
                        console.log(commands);
                        console.log(command);
                        console.log(raises);
                    }
                }
                // console.log(command);
                // console.log(commands);
            },

            redo: function () {
                var command;
                
                do {
                    command = raises.pop();
                } while (!!command && !!command.log);

                // var command = raises.pop();

                if (!!command) {
                    command.redo();
                    commands.push(command);
                }
                // console.log(commands);
            },

            setOptions: function (obj) {
                for (var keys in obj) !!options[keys] ? options[keys] = obj[keys] : null;
            },

            actionCommand: function (amendment, isAdd, count) {
                if (amendment == 'undo') {
                    if (isAdd) do commands.push({ log: 'empty' }); while (--count > 0);
                    else do commands.pop(); while (--count > 0);
                } else if (amendment == 'redo') {
                    if (isAdd) do raises.push({ log: 'empty' }); while (--count > 0);
                    else do raises.pop(); while (--count > 0);
                }
            },

            isNextCommand: function () {
                // console.log(raises);
                // console.log(commands);
                var obj = raises[raises.length - 1];
                return !!obj && !!obj.log;
            }
        }
    };

})();