(function () {
    'use strict';

    L.AD = L.AD || {
        version: '5.0.0',
        initialize() {
            this.addInitHooks();
        },
        addInitHooks() {
            function initLayerGroup() {
                this.ad = new L.AD.Edit.LayerGroup(this);
            }

            L.LayerGroup.addInitHook(initLayerGroup);

            function initPolygon() {
                this.ad = new L.AD.Edit.Poly(this);
            }

            L.Polygon.addInitHook(initPolygon);

            function initPolyline() {
                this.ad = new L.AD.Edit.Line(this);
            }

            L.Polyline.addInitHook(initPolyline);

            function initCircle() {
                this.ad = new L.AD.Edit.Circle(this);
            }
    
            L.Circle.addInitHook(initCircle);

            function initMap() {
                this.ad = new L.AD.Map(this);
            }

            L.Map.addInitHook(initMap);
        }
    };

    L.AD.initialize();

})();