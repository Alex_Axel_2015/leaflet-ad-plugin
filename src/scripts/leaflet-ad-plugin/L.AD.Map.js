(function () {
    'use strict';

    L.AD.Map = L.Class.extend({
        initialize(map) {
            this.map = map;
            this.Draw = new L.AD.Draw(map);
            this.Tooltip = new L.AD.Tooltip(map);          
            this.ComandDraw = new L.AD.Comand();            

            var _this = this; // keypress draw event
            function _keyDownDraw(e) {
                var key = e.which || e.keyCode, // keyCode detection
                    ctrl = e.ctrlKey ? e.ctrlKey : ((key === 17) ? true : false), // ctrl detection
                    shift = e.shiftKey ? e.shiftKey : ((key === 16) ? true : false); // shift detection

                if (key == 26 && ctrl && shift) {
                    _this.ComandDraw.redo();
                    _this.toggleGlobalEditMode();
                    _this.toggleGlobalEditMode();
                } else if (key == 26 && ctrl) {
                    _this.ComandDraw.undo();
                    _this.toggleGlobalEditMode();
                    _this.toggleGlobalEditMode();
                }
            }

            this.map._container.addEventListener('keypress', _keyDownDraw);
        },
        enableDraw(shape = 'Poly', options) {
            this.Draw.enable(shape, options);
        },
        disableDraw(shape = 'Poly') {
            this.Draw.disable(shape);
        },
        setPathOptions(options) {
            this.Draw.setPathOptions(options);
        },
        removeLayer(e) {
            const layer = e.target;
            if (!layer._layers && !layer.pm.dragging()) {
                e.target.remove();
            }
        },

        globalEditEnabled() {
            return this._globalEditMode;
        },
        toggleGlobalEditMode(options = { snappable: true, draggable: true }) {
            // find all layers that are or inherit from Polylines...
            let layers = [];
            this.map.eachLayer((layer) => {
                if (layer instanceof L.Polyline || layer instanceof L.Marker || layer instanceof L.Circle) {
                    layers.push(layer);
                }
            });

            // filter out layers that don't have the leaflet.pm instance
            layers = layers.filter(layer => !!layer.ad);

            // filter out everything that's leaflet.pm specific temporary stuff
            layers = layers.filter(layer => !layer._pmTempLayer);

            if (this.globalEditEnabled()) {
                // disable

                this._globalEditMode = false;

                layers.forEach((layer) => {
                    layer.ad.disable();
                });
            } else {
                // enable

                this._globalEditMode = true;

                layers.forEach((layer) => {
                    layer.ad.enable(options);
                });
            }

            // toggle the button in the toolbar
            // this.Toolbar.toggleButton('editPolygon', this._globalEditMode);
        },
    });

})();