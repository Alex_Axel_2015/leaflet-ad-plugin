(function () {
    'use strict';

    L.AD.Draw = L.Class.extend({
        includes: [], //SnapMixin
        options: {
            snappable: true,
            snapDistance: 20,
            finishOnDoubleClick: true,
            drawControlTooltips: false,
            templineStyle: {
                className: 'templineCustomClass'
                // color: 'red',
                // strokeWidth: 5
            },
            hintlineStyle: {
                className: 'hintlineCustomClass',
                // color: 'green',
                dashArray: [7, 7],
                // strokeWidth: 5
            },
            markerStyle: {
                draggable: true,
            },
        },
        initialize(map) {
            this._map = map;            
            // this.shapes = ['Poly', 'Line', 'Marker', 'Circle'];
            this.shapes = ['Poly', 'Line', 'Circle'];            

            // initiate drawing class for our shapes
            this.shapes.forEach((shape) => {
                this[shape] = new L.AD.Draw[shape](this._map);
            });

            this.is_move = false;
            this.is_remove = false;
        },
        setPathOptions(options) {
            this.options.pathOptions = options;
        },
        enable(shape, options) {
            if (!shape) {
                throw new Error(`Error: Please pass a shape as a parameter. Possible shapes are: ${this.getShapes().join(',')}`);
            }
            this.disable();
            this[shape].enable(options);
        },
        disable() {
            this.shapes.forEach((shape) => {
                this[shape].disable();
            });
        },

        updateEditMode() {
            this._map.ad.toggleGlobalEditMode();
            this._map.ad.toggleGlobalEditMode();
        },

        stepLayers: {
            steck: [],
            essence: []
        }
    });

})();