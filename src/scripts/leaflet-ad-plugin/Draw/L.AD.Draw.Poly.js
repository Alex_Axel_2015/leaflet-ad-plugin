L.AD.Draw.Poly = L.AD.Draw.Line.extend({

    initialize(map) {
        this._map = map;
        this._shape = 'Poly';
        this.toolbarButtonName = 'drawPolygon';
    },
    _finishShape() {
        // get coordinates, create the leaflet shape and add it to the map
        const coords = this._layer.getLatLngs();

        if (coords.length > 2) {
            const polygonLayer = L.polygon(coords, this.options.pathOptions);

            //L.AD.ComandDraw
            var __calback = () => {
                this._map.ad.ComandDraw.actionCommand('undo', false);

                // if (this.stepLayers === undefined) this.stepLayers = { steck: [], essence: [polygonLayer] };
                // else this.stepLayers.essence.push(polygonLayer);
                this.stepLayers.essence.push(polygonLayer);

                polygonLayer.addTo(this._map);
                this._map.removeLayer(this.polygonLayerDemo); // demoPolygon remove                
                this.disable(true); // disable drawing
                this._map.ad.enableDraw('Poly', {});
                this.updateEditMode();

                // fire the pm:create event and pass shape and layer
                this._map.fire('ad:create', {
                    shape: this._shape,
                    layer: polygonLayer
                });
            };
            var __undo = () => {
                polygonLayer._latlngs.splice(polygonLayer._latlngs.length - 1, 1);
                if (polygonLayer._latlngs.length < 3) {
                    this._map.removeLayer(polygonLayer);

                    this.stepLayers.steck.push(polygonLayer);
                    this.stepLayers.essence.pop();

                    this._map.ad.ComandDraw.actionCommand('undo', false, 2);
                    this._map.ad.ComandDraw.actionCommand('redo', true);

                } else {
                    polygonLayer.redraw();
                }
            };
            var __redo = () => {
                if (!polygonLayer._map) { // его точно не будет
                    polygonLayer.addTo(this._map);

                    this.stepLayers.essence.push(polygonLayer);
                    this.stepLayers.steck.pop();

                    this._map.ad.ComandDraw.actionCommand('undo', true, 2);
                    this._map.ad.ComandDraw.actionCommand('redo', false);
                }
                
                polygonLayer.addLatLng(coords[coords.length - 1]);
                polygonLayer.redraw();
            };
            var createPolygon = new Comand(__calback, __undo, __redo, null);
            this._map.ad.ComandDraw.execute(createPolygon);
        }
    },
    _maybeFinish(e) {
        e.target.options.id === this.polygonLayerDemo.getLatLngs().length ? this._finishShape() : null;
    },
    _createMarker(latlng, first) {
        // create the new marker
        var index = this._layerGroup.getLayers().length - 3;
        const marker = new L.Marker(latlng, {
            id: index,
            draggable: false,
            icon: L.divIcon({ className: 'marker-icon', html: '<div class="icon-index" index="' + index + '"></div>' }),
        });

        marker.on('mousedown', () => { this._map.off('mousedown', this._createVertex, this) }, this); // click
        marker.on('mouseout', () => { this._refreshEventCreateVertext() }, this);

        // mark this marker as temporary
        marker._pmTempLayer = true;

        // add it to the map
        // this._layerGroup.addLayer(marker);

        //L.AD.ComandDraw
        var __calback = () => {
            this._layer.addLatLng(latlng); // with vertex
            this._layerGroup.addLayer(marker);
            if (index > 2) {
                if (!!this.polygonLayerDemo) this._map.removeLayer(this.polygonLayerDemo);
                this.polygonLayerDemo = L.polygon(this._layer.getLatLngs(), this.options.pathOptions).addTo(this._map);
                marker.on('click', this._maybeFinish, this);
            }

            if (first) {
                marker.on('click', this._finishShape, this);
            }
            this._syncHintLine();

            this.stepLayers.steck = []; // if (this.stepLayers !== undefined) this.stepLayers.steck = [];
        };
        var __undo = () => {
            if (this._layer._latlngs.length > 0) {
                // startvertex
                const lengthL = this._layer._latlngs.length - 1;
                this._layer._latlngs.splice(lengthL, 1);
                this._layer.redraw();
                // endvertex
                this._layerGroup.removeLayer(marker);
                if (!!this.polygonLayerDemo) this._map.removeLayer(this.polygonLayerDemo);
                if ((this._layerGroup.getLayers().length - 3) > 2) {
                    this.polygonLayerDemo = L.polygon(this._layer.getLatLngs(), this.options.pathOptions).addTo(this._map);
                }
                this._syncHintLine();
            } else {

                let l_polygon = this.stepLayers.essence[this.stepLayers.essence.length - 1];
                let crds = l_polygon._latlngs;
                crds.splice(crds.length - 1, 1);
                if (crds.length < 3) {
                    this._map.removeLayer(l_polygon);
                    
                    this.stepLayers.steck.push(l_polygon);
                    this.stepLayers.essence.pop();

                    this._map.ad.ComandDraw.actionCommand('undo', false, 2);
                    this._map.ad.ComandDraw.actionCommand('redo', true);
                } else {
                    l_polygon.redraw();
                }
            }
        };
        var __redo = () => {
            if (this._layer._latlngs.length == 0 && index > 2) {
                let l_polygon = this.stepLayers.essence[this.stepLayers.essence.length - 1];
                let isEmpty = this._map.ad.ComandDraw.isNextCommand();
                
                if (!this.stepLayers.essence.length || isEmpty) {

                    l_polygon = this.stepLayers.steck.pop();
                    this.stepLayers.essence.push(l_polygon);

                    l_polygon.addTo(this._map);
                    this._map.ad.ComandDraw.actionCommand('undo', true, 2);
                    this._map.ad.ComandDraw.actionCommand('redo', false);
                }

                l_polygon.addLatLng(latlng);
                l_polygon.redraw();

            } else {
                __calback();
            }
        };
        var addPoint = new Comand(__calback, __undo, __redo, null);
        this._map.ad.ComandDraw.execute(addPoint);
    }
});
