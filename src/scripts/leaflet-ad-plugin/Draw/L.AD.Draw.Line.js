(function () {
    'use strict';

    L.AD.Draw.Line = L.AD.Draw.extend({
        initialize(map) {
            this._map = map;            
            this._shape = 'Line';
            this.toolbarButtonName = 'drawPolyline';
            // map.ad.Tooltip.updateContent(this._getTooltipText()); // test
        },
        enable(options) {
            // TODO: Think about if these options could be passed globally for all
            // instances of L.PM.Draw. So a dev could set drawing style one time as some kind of config
            L.Util.setOptions(this, options);

            // enable draw mode
            this._enabled = true;

            // create a new layergroup
            this._layerGroup = new L.LayerGroup();
            this._layerGroup.addTo(this._map);

            // this is the polyLine that'll make up the polygon
            this._layer = L.polyline([], this.options.templineStyle);
            this._layer._pmTempLayer = true;
            this._layerGroup.addLayer(this._layer);

            // this is the hintline from the mouse cursor to the last marker
            this._hintline = L.polyline([], this.options.hintlineStyle);
            this._hintline._pmTempLayer = true;
            this._layerGroup.addLayer(this._hintline);

            // для полигона
            if (this._shape == "Poly") {
                this._hintlinePoly = L.polyline([], this.options.hintlineStyle);
                this._hintlinePoly._pmTempLayer = true;
                this._layerGroup.addLayer(this._hintlinePoly);
            }
            ////////////////////////////////

            // this is the hintmarker on the mouse cursor
            this._hintMarker = L.marker([0, 0], {
                // icon: L.divIcon({ className: 'marker-icon cursor-marker' }),
                icon: L.divIcon({ className: 'cursor-marker' }),
            });
            this._hintMarker._pmTempLayer = true;
            this._layerGroup.addLayer(this._hintMarker);

            // show the hintmarker if the option is set
            if (this.options.cursorMarker) {
                L.DomUtil.addClass(this._hintMarker._icon, 'visible');
            }

            // change map cursor
            this._map._container.style.cursor = 'crosshair';

            // create a polygon-point on click
            this._map.on('mousedown', this._createVertex, this); // click

            // finish on double click
            if (this.options.finishOnDoubleClick) {
                this._map.on('dblclick', this._finishShape, this);
            }

            // sync hint marker with mouse cursor
            this._map.on('mousemove', this._syncHintMarker, this);

            // sync the hintline with hint marker
            this._hintMarker.on('move', this._syncHintLine, this);

            // fire drawstart event
            this._map.fire('ad:drawstart', { shape: this._shape });

            // toggle the draw button of the Toolbar in case drawing mode got enabled without the button
            // this._map.ad.Toolbar.toggleButton(this.toolbarButtonName, true);

            // an array used in the snapping mixin.
            // TODO: think about moving this somewhere else?
            this._otherSnapLayers = [];

            this._map.ad.Draw.options.drawControlTooltips = true;
        },
        disable(isFinish) {
            // disable draw mode

            // cancel, if drawing mode isn't even enabled
            if (!this._enabled) {
                return;
            }

            this._enabled = false;

            // reset cursor
            this._map._container.style.cursor = 'default';

            // unbind listeners
            this._map.off('mousedown', this._createVertex, this); // click
            this._map.off('mousemove', this._syncHintMarker, this);
            this._map.off('dblclick', this._finishShape, this);

            // For clear command after switch draw element
            if (!isFinish && this._layer._latlngs.length) this._map.ad.ComandDraw.actionCommand('undo', false, this._layer._latlngs.length);
            if (!!this.polygonLayerDemo) this._map.removeLayer(this.polygonLayerDemo); // demoPolygon remove
            this._layer._latlngs = [];

            // remove layer
            this._map.removeLayer(this._layerGroup);

            // fire drawend event
            this._map.fire('ad:drawend', { shape: this._shape });

            // toggle the draw button of the Toolbar in case drawing mode got disabled without the button
            // this._map.ad.Toolbar.toggleButton(this.toolbarButtonName, false);

            // cleanup snapping
            if (this.options.snappable) {
                // this._cleanupSnapping(); // SNAPPING GOOOOOOO
            }

            this._map.ad.Draw.options.drawControlTooltips = false;
        },
        enabled() {
            return this._enabled;
        },
        toggle(options) {
            if (this.enabled()) {
                this.disable();
            } else {
                this.enable(options);
            }
        },

        _getTooltipText() {
            return {
                text: 'Tooltip text!',
                // subtext: 'Subtext'
            };
        },

        _syncHintLine() {
            const polyPoints = this._layer.getLatLngs();
            var arr_hintline = [], arr_hintlinePoly = [];

            if (polyPoints.length > 0) {
                const lastPolygonPoint = polyPoints[polyPoints.length - 1];

                arr_hintline = [lastPolygonPoint, this._hintMarker.getLatLng()];
                arr_hintlinePoly = [polyPoints[0], this._hintMarker.getLatLng()];
            }

            switch (this._shape) {
                case 'Poly': this._hintlinePoly.setLatLngs(arr_hintlinePoly);
                case 'Line': this._hintline.setLatLngs(arr_hintline); break;
            }            
        },
        _syncHintMarker(e) {
            // console.log('_sinc');
            // console.log(this);
            // console.log(e);
            // move the cursor marker
            this._hintMarker.setLatLng(e.latlng);

            // if snapping is enabled, do it
            if (this.options.snappable) {
                const fakeDragEvent = e;
                fakeDragEvent.target = this._hintMarker;
                // this._handleSnapping(fakeDragEvent); // SNAPPING GOOOOOOO
            }

            /////

            this._map.ad.Tooltip.updatePosition(e.latlng);
            // this._map.ad.Tooltip.updateContent({text: e.latlng});
        },
        _createVertex(e) {
            if (e.originalEvent.which == 1) {
                // console.log('create vertex');
                // assign the coordinate of the click to the hintMarker, that's necessary for
                // mobile where the marker can't follow a cursor
                if (!this._hintMarker._snapped) {
                    this._hintMarker.setLatLng(e.latlng);
                }

                // get coordinate for new vertex by hintMarker (cursor marker)
                const latlng = this._hintMarker.getLatLng();

                // check if the first and this vertex have the same latlng
                if (latlng.equals(this._layer.getLatLngs()[0])) {
                    // yes? finish the polygon
                    this._finishShape();

                    // "why?", you ask? Because this happens when we snap the last vertex to the first one
                    // and then click without hitting the last marker. Click happens on the map
                    // in 99% of cases it's because the user wants to finish the polygon. So...
                    return;
                }

                // is this the first point?
                const first = this._layer.getLatLngs().length === 0;

                // this._layer.addLatLng(latlng);
                this._createMarker(latlng, first);
                this._syncHintLine(); // this._hintline.setLatLngs([latlng, latlng]);
            }
        },
        _finishShape() {
            // get coordinates, create the leaflet shape and add it to the map
            const coords = this._layer.getLatLngs();
            if (coords.length > 1) {
                const polylineLayer = L.polyline(coords, this.options.pathOptions);

                // L.AD.ComandDraw
                var __calback = () => {
                    this._map.ad.ComandDraw.actionCommand('undo', false);
                    // if (this.stepLayers === undefined) this.stepLayers = { steck: [], essence: [polylineLayer] };
                    // else this.stepLayers.essence.push(polylineLayer);
                    this.stepLayers.essence.push(polylineLayer);

                    polylineLayer.addTo(this._map);

                    // disable drawing
                    this.disable(true);
                    this._map.ad.enableDraw('Line', {});
                    this.updateEditMode();

                    // fire the pm:create event and pass shape and layer
                    this._map.fire('ad:create', {
                        shape: this._shape,
                        layer: polylineLayer,
                    });
                };
                var __undo = () => {
                    polylineLayer._latlngs.splice(polylineLayer._latlngs.length - 1, 1);
                    if (polylineLayer._latlngs.length < 2) {
                        this._map.removeLayer(polylineLayer);

                        this.stepLayers.steck.push(polylineLayer);
                        this.stepLayers.essence.pop();

                        this._map.ad.ComandDraw.actionCommand('undo', false);
                        this._map.ad.ComandDraw.actionCommand('redo', true);

                    } else {
                        polylineLayer.redraw();
                    }
                };
                var __redo = () => {
                    if (!polylineLayer._map) { // его точно не будет
                        polylineLayer.addTo(this._map);

                        this.stepLayers.essence.push(polylineLayer);
                        this.stepLayers.steck.pop();

                        this._map.ad.ComandDraw.actionCommand('undo', true);
                        this._map.ad.ComandDraw.actionCommand('redo', false);
                    }

                    polylineLayer.addLatLng(coords[coords.length - 1]);
                    polylineLayer.redraw();
                };
                var addPoint = new Comand(__calback, __undo, __redo, null);
                this._map.ad.ComandDraw.execute(addPoint);
            }
        },
        _createMarker(latlng, first) {
            // create the new marker
            var index = this._layerGroup.getLayers().length - 2;
            const marker = new L.Marker(latlng, {
                id: index,
                draggable: false,
                icon: L.divIcon({ className: 'marker-icon', html: '<div class="icon-index" index="' + index + '"></div>' }),
            });

            marker.on('mousedown', () => { this._map.off('mousedown', this._createVertex, this) }, this); // click
            marker.on('mouseout', () => { this._refreshEventCreateVertext() }, this);

            marker._pmTempLayer = true;

            // L.AD.ComandDraw
            var __calback = () => {
                // add it to the map
                this._layer.addLatLng(latlng); // with vertex
                this._layerGroup.addLayer(marker);

                // a click on any marker finishes this shape            
                marker.on('click', this._finishShape, this);

                this._syncHintLine();

                this.stepLayers.steck = []; // if (this.stepLayers !== undefined) this.stepLayers.steck = [];

                return marker;
            };
            var __undo = () => {
                if (this._layer._latlngs.length > 0) {
                    // startvertex
                    const lengthL = this._layer._latlngs.length - 1;
                    this._layer._latlngs.splice(lengthL, 1);
                    this._layer.redraw();
                    // endvertex
                    this._layerGroup.removeLayer(marker);
                    this._syncHintLine();
                } else {
                    let l_polyline = this.stepLayers.essence[this.stepLayers.essence.length - 1];
                    let crds = l_polyline._latlngs;
                    crds.splice(crds.length - 1, 1);
                    if (crds.length < 2) {
                        this._map.removeLayer(l_polyline);

                        this.stepLayers.steck.push(l_polyline);
                        this.stepLayers.essence.pop();

                        this._map.ad.ComandDraw.actionCommand('undo', false);
                        this._map.ad.ComandDraw.actionCommand('redo', true);
                    } else {
                        l_polyline.redraw();
                    }
                }
            };
            var __redo = () => {
                if (this._layer._latlngs.length == 0 && index > 1) {
                    let l_polyline = this.stepLayers.essence[this.stepLayers.essence.length - 1];
                    let isEmpty = this._map.ad.ComandDraw.isNextCommand();

                    if (!this.stepLayers.essence.length || isEmpty) {

                        l_polyline = this.stepLayers.steck.pop();
                        this.stepLayers.essence.push(l_polyline);

                        l_polyline.addTo(this._map);

                        this._map.ad.ComandDraw.actionCommand('undo', true);
                        this._map.ad.ComandDraw.actionCommand('redo', false);
                    }

                    l_polyline.addLatLng(latlng);
                    l_polyline.redraw();

                } else {
                    __calback();
                }
            };
            var addPoint = new Comand(__calback, __undo, __redo, null);
            this._map.ad.ComandDraw.execute(addPoint);
        },
        _refreshEventCreateVertext() {
            this._map
                .off('mousedown', this._createVertex, this)
                .on('mousedown', this._createVertex, this);
        }
    });

})();