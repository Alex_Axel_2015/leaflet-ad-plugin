(function () {
    'use strict';

    function HomeCtrl() {

    }

    angular
        .module('app.home', [])
        .controller('HomeController', HomeCtrl);

})();