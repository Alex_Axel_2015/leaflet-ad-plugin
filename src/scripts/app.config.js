(function() {
    'use strict';

    angular
        .module('app')
        .config(configure);

    configure.$inject =
        ['$locationProvider', '$httpProvider', '$routeProvider'];

    function configure ($locationProvider, $httpProvider, $routeProvider) {
        // $locationProvider.html5Mode({
        //     enabled: true,
        //     requireBase: false
        // });

        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.headers.common = 'Content-Type: application/json';
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $routeProvider.when("/home", {
            template: "<empty></empty>",
            controller: 'HomeController',
            reloadOnSearch: false
        });

        $routeProvider.otherwise({redirectTo: '/home'});
    }

})();