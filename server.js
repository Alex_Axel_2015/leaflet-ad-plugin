var serverFactory = require('spa-server');

var server = serverFactory.create({
    path: './',
    port: 9000,
    fallback: {
        'text/html' : '/test/index.html'
    }
});

server.start();