module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      'dist/scripts/libs.js',
      'node_modules/angular-mocks/angular-mocks.js',
      // 'dist/scripts/app.js',
          
      'src/scripts/app.module.js',
      'src/scripts/app.config.js',
      'src/scripts/app.controller.js',
      'src/scripts/app.constant.js',
      'src/scripts/app.templates.js',
      'src/scripts/services/*.js',
      'src/scripts/page.js',
      'src/scripts/components/*.js',

      'src/scripts/**/*.spec.js'
    ],

    exclude: [],
    preprocessors: {},

    preprocessors: {
      'src/scripts/**/!(*.spec).js': ['coverage']
    },
    coverageReporter: {
      type: 'html',
      dir: 'coverage/'
    },

    // possible values: 'dots', 'progress'
    reporters: ['progress', 'coverage'],
    port: 9876,
    colors: true,

    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,
    browsers: ['Chrome'],

    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,
    concurrency: Infinity
  })
}
